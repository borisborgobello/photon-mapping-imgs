# Photon mapping images by Boris Borgobello & Thomas Feigler

This repos bares our results for the git repo :
git@github.com:borisborgobello/photon-mapping.git

We share with you here glitches, bugs, and other results we obtained during the development a few years ago.

Thanks for reading !
Boris & Thomas

![Alt Text](https://bitbucket.org/borisborgobello/photon-mapping-imgs/raw/e04f3bfe92d7e729f7443e71d09806a611e25476/anim/anim_photon.gif)
